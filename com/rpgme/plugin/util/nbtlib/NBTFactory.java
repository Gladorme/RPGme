package com.rpgme.plugin.util.nbtlib;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


@SuppressWarnings({"rawtypes", "unchecked"})
public final class NBTFactory {

	private NBTFactory() { }
	
	/**
	 * The version part used in class names for NMS and CraftBukkit
	 */
	public static final String VERSION = Bukkit.getServer().getClass().getPackage().getName().substring(23);
	
	/**
	 * The <strong>package name</strong> for NMS classes
	 */
	public static final String NMS_PATH = "net.minecraft.server." + VERSION;
	
	/**
	 * The base <strong>package name</strong> for CraftBukkit classes
	 */
	public static final String CRAFT_PATH = "org.bukkit.craftbukkit." + VERSION;
	
	
	/**
	 * Checks if an ItemStack has an NBT tag attached
	 * @param item the item to check
	 * @return true if item is not null and has an NBT tag
	 */
	public static boolean hasTag(ItemStack item) {
		// Material air cannot hold a tag
		if(item == null || item.getType() == Material.AIR)
			return false;
		
		try {
			Class<?> craftClazz = Class.forName(CRAFT_PATH + ".inventory.CraftItemStack");

			Object handle = craftClazz.getMethod("asNMSCopy", ItemStack.class).invoke(null, item);
			
			if(handle == null)
				return false;
			
			Field tagField = handle.getClass().getDeclaredField("tag");
			tagField.setAccessible(true);

			Object tag = tagField.get(handle);
			return tag != null;
			
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	public static CompoundTag getFrom(ItemStack item) {
		return get(item, false);
	}
	
	public static CompoundTag getOrEmpty(ItemStack item) {
		return get(item, true);
	}
	
	private static CompoundTag get(ItemStack item, boolean orEmpty) {
		if(item == null || item.getType() == Material.AIR)
			return orEmpty ? new CompoundTag() : null;

		try {
			Class<?> craftClazz = Class.forName(CRAFT_PATH + ".inventory.CraftItemStack");

			Object handle = craftClazz.getMethod("asNMSCopy", ItemStack.class).invoke(null, item);
			if(handle == null)
				return null;

			Field tagField = handle.getClass().getDeclaredField("tag");
			tagField.setAccessible(true);

			Object tag = tagField.get(handle);
			
			return tag == null ? (orEmpty ? new CompoundTag() : null) : toCompoundTag(tag);

		} catch(Exception e) {
			e.printStackTrace();
			return orEmpty ? new CompoundTag() : null;
		}
	}

	public static CompoundTag copyOf(Entity entity) {
		if(entity == null)
			return null;
		
		try {
			Object nmsTag = Class.forName(NMS_PATH+".NBTTagCompound").newInstance();
			
			Class<?> craftentityClazz = Class.forName(CRAFT_PATH + ".entity.CraftEntity");
			Object handle = craftentityClazz.getMethod("getHandle").invoke(craftentityClazz.cast(entity));
			
			/*
			 * If this code breaks, it's likely to be here
			 * As of v1_8_R3 method 'e' puts entity information into the nms tag
			 */
			handle.getClass().getMethod("e", nmsTag.getClass()).invoke(handle, nmsTag);
			
			return toCompoundTag(nmsTag);

		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}


	}

	public static ItemStack setCompoundTag(ItemStack item, CompoundTag tag) {
		if(item == null || item.getType() == Material.AIR)
			throw new NullPointerException("Item may not be null or air");
		if(tag == null)
			throw new NullPointerException("Compound tag may not be null");

		try {
			Class<?> craftClazz = Class.forName(CRAFT_PATH+".inventory.CraftItemStack");

			Object handle = craftClazz.getMethod("asNMSCopy", ItemStack.class).invoke(null, item);

			Field tagField = handle.getClass().getDeclaredField("tag");
			tagField.setAccessible(true);

			tagField.set(handle, toNMSTag(tag));

			return (ItemStack) craftClazz.getMethod("asBukkitCopy", handle.getClass()).invoke(null, handle);
		} catch(Exception e) {
			e.printStackTrace();
			return item;
		}
	}

	public static void setEntityAttributes(Entity entity, CompoundTag tag) {
		if(entity == null)
			throw new NullPointerException("Entity may not be null");
		if(tag == null)
			throw new NullPointerException("Compound tag may not be null");

		try {
						
			Class<?> craftentityClazz = Class.forName(CRAFT_PATH + ".entity.CraftEntity");
			Object handle = craftentityClazz.getMethod("getHandle").invoke(craftentityClazz.cast(entity));
			
			Object nmsTag = toNMSTag(tag);

			/*
			 * If this code breaks, it's likely to be here
			 * As of v1_8_R3 method 'f' reads entity information from the nms tag
			 * and applies it to the entity
			 */
			handle.getClass().getMethod("f", nmsTag.getClass()).invoke(handle, nmsTag);
			
		} catch(Exception e) {
			e.printStackTrace();
		}


	}

	/*
	 * NMS to mirrored (for getters)
	 */

	public static CompoundTag toCompoundTag(Object nmsCompound) throws Exception {

		Field mapField =  nmsCompound.getClass().getDeclaredField("map");
		mapField.setAccessible(true);
		Object map = mapField.get(nmsCompound);

		CompoundTag tag = new CompoundTag();

		Set<Entry> entryset = ((Map)map).entrySet();

		for(Entry e : entryset) {

			String key = (String) e.getKey();
			Object tagBase = e.getValue();

			tag.putTag(key, buildTag(key, tagBase));

		}
		return tag;
	}

	private static Tag buildTag(String key, Object tagBase) throws Exception {

		byte id = (byte) tagBase.getClass().getMethod("getTypeId").invoke(tagBase);

		if(id == NBTConstants.TYPE_COMPOUND) {

			return toCompoundTag(tagBase);

		} else if(id == NBTConstants.TYPE_LIST) {

			Field typeField = tagBase.getClass().getDeclaredField("type");
			typeField.setAccessible(true);

			byte type = (byte) typeField.get(tagBase);
			ListTag tag = new ListTag(key);

			Field listField = tagBase.getClass().getDeclaredField("list");
			listField.setAccessible(true);

			for(Object obj : (List) listField.get(tagBase)) {
				tag.add(buildTag("", obj));

			}
			return tag;

		} else {

			Field dataField = tagBase.getClass().getDeclaredField("data");
			dataField.setAccessible(true);
			Object data = dataField.get(tagBase);

			switch(id) {
			case NBTConstants.TYPE_END: return new EndTag();
			case NBTConstants.TYPE_BYTE: return new ByteTag(key, (byte)data);
			case NBTConstants.TYPE_SHORT: return new ShortTag(key, (short)data);
			case NBTConstants.TYPE_INT: return new IntTag(key, (int)data);
			case NBTConstants.TYPE_LONG: return new LongTag(key, (long)data);
			case NBTConstants.TYPE_FLOAT: return new FloatTag(key, (float)data);
			case NBTConstants.TYPE_DOUBLE: return new DoubleTag(key, (double)data);
			case NBTConstants.TYPE_BYTE_ARRAY: return new ByteArrayTag(key, (byte[])data);
			case NBTConstants.TYPE_STRING: return new StringTag(key, (String)data);
			case NBTConstants.TYPE_INT_ARRAY: return new IntArrayTag(key, (int[])data);
			default: return null;
			}

		}
	}

	/*
	 * Mirrored to NMS (for setters)
	 */

	public static Object toNMSTag(CompoundTag tagCompound) throws Exception {

		Object nmsTag = Class.forName(NMS_PATH+".NBTTagCompound").newInstance();
		Class nbtbaseClass = Class.forName(NMS_PATH + ".NBTBase");

		for(Entry<String, Tag> entry : tagCompound.entrySet()) {

			nmsTag.getClass().getMethod("set", String.class, nbtbaseClass).invoke(nmsTag, entry.getKey(), buildNMSTag(entry.getValue()));

		}

		return nmsTag;
	}

	private static Object buildNMSTag(Tag tag) throws Exception {

		byte id = tag.getTypeId();

		if(id == NBTConstants.TYPE_COMPOUND) {

			return toNMSTag((CompoundTag)tag);

		} else if(id == NBTConstants.TYPE_LIST) {

			Class listClazz = Class.forName(NMS_PATH + ".NBTTagList");
			Class nbtbaseClass = Class.forName(NMS_PATH + ".NBTBase");

			Object nmsList = listClazz.newInstance();
			for(Tag child : ((ListTag)tag).getValue()) {
				listClazz.getMethod("register", nbtbaseClass).invoke(nmsList, buildNMSTag(child));
			}
			return nmsList;
		}

		StringBuilder sb = new StringBuilder(NMS_PATH).append(".NBTTag");

		switch(tag.getTypeId()) {
		case NBTConstants.TYPE_END: sb.append("End"); break;
		case NBTConstants.TYPE_BYTE: sb.append("Byte"); break;
		case NBTConstants.TYPE_SHORT: sb.append("Short"); break;
		case NBTConstants.TYPE_INT: sb.append("Int"); break;
		case NBTConstants.TYPE_LONG: sb.append("Long"); break;
		case NBTConstants.TYPE_FLOAT: sb.append("Float"); break;
		case NBTConstants.TYPE_DOUBLE: sb.append("Double"); break;
		case NBTConstants.TYPE_BYTE_ARRAY: sb.append("ByteArray"); break;
		case NBTConstants.TYPE_STRING: sb.append("String"); break;
		case NBTConstants.TYPE_INT_ARRAY: sb.append("IntArray"); break;
		}

		Class<?> valueType = tag.getValue().getClass();
        try {
            valueType = (Class<?>) valueType.getField("TYPE").get(null);
        } catch (NoSuchFieldException ignore) { /* not a primitive */ }

		Class<?> clazz = Class.forName(sb.toString());
        Constructor<?> constructor = clazz.getConstructor(valueType);
        return constructor.newInstance(tag.getValue());
	}


}
