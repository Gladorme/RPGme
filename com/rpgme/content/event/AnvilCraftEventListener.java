package com.rpgme.content.event;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;


public class AnvilCraftEventListener extends Listener<RPGme> {

	public AnvilCraftEventListener(RPGme plugin) {
		super(plugin);
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent e) {

		if(e.getInventory().getType() != InventoryType.ANVIL ||
                e.getSlot() != 2 ||
                e.getClickedInventory() != e.getView().getTopInventory() ||
                !e.getAction().name().startsWith("PICKUP")) {
			return;
		}
		
//		AnvilCraftEvent event = new AnvilCraftEvent(e.getView(), (Player)e.getWhoClicked());
//		Bukkit.getPluginManager().callEvent(event);
	}
	

}
