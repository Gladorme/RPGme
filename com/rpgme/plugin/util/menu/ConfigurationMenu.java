package com.rpgme.plugin.util.menu;

import com.google.common.collect.Maps;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GUI;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.menu.Settings.Setting;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class ConfigurationMenu extends GUI<RPGme> {

    private static final IntMap<Class<? extends Setting>> settingClasses = new IntMap<>(18);
	//private static Map<Integer, Class<? extends Setting>> settingClasses = Maps.newHashMap();
	private static int rows = 1;

	public static void registerSetting(int slot, Class<? extends Setting> clazz) {
        unregisterSetting(clazz);

	    while(settingClasses.containsKey(slot)) {
	        slot++;
        }
        rows = Math.max(rows, (slot / 9) + 1);
		settingClasses.put(slot, clazz);
	}

	public static void unregisterSetting(Class<? extends Setting> clazz) {
        int slot = settingClasses.findKey(clazz, true, -1);
        if(slot >= 0) {
            settingClasses.remove(slot);
        }
    }

	private final RPGPlayer rpPlayer;
	private final Map<Integer, Setting> settings = Maps.newHashMap();

	public ConfigurationMenu(RPGme plugin, Player p) {
		super(plugin, p, plugin.getMessages().getMessage("ui_settings_title"), rows);
		rpPlayer = plugin.getPlayer(player);
		addItems();
	}


	@Override
	public void addItems() {

	    for(IntMap.Entry<Class< ? extends Setting>> e : settingClasses.entries()) {
			Setting set = null;
			try {
				set = e.value.getConstructor(RPGPlayer.class).newInstance(rpPlayer);
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e1) {
				plugin.getLogger().severe("Error instantiating setting "+e.value + '(' + e1.getMessage() + ')');
				continue;
			}

			settings.put(e.key, set);

			addItem(e.key, set.getAsItem());

		}
	}

	@Override
	public void doAction(int slot) {
		Setting setting = settings.get(slot);
		if(setting != null)
			addItem(slot, setting.onToggle());

	}


}
