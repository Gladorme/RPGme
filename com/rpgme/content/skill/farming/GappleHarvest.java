package com.rpgme.content.skill.farming;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.ItemSpawnEvent;

import java.util.List;

public class GappleHarvest extends Ability<Farming> {
	
	private int unlocked, upgraded;
	private Scaler chance, godChance;

	public GappleHarvest(Farming skill) {
		super(skill, "Gapple Harvest", Farming.ABILITY_GAPPLE_HARVEST);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 25)
                .addValue("upgrade", 60);

        ConfigHelper.injectNotification(messages, getClass(), 1);
        ConfigHelper.injectNotification(messages, getClass(), 2);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = getConfig().getInt("unlocked");
        upgraded = getConfig().getInt("upgrade");

        chance = new Scaler(unlocked, 10, 100, 90);
        godChance = new Scaler(upgraded, 10, 100, 25);

        addNotification(unlocked, Notification.ICON_UNLOCK, "Gapple Harvest", ConfigHelper.getNotification(messages, getClass(), 1));
        addNotification(unlocked, Notification.ICON_UPGRADE, "Gapple Harvest", ConfigHelper.getNotification(messages, getClass(), 2));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlocked) {
			list.add("Golden Apple chance:"+chance.readableScale(forlevel) + '%');
			if(forlevel >= upgraded) {
				list.add("God Apple chance:"+godChance.readableScale(forlevel) + '%');
			}
		}
		
	}
	
	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAppleSpawn(ItemSpawnEvent e) {
		if(e.getEntity().getItemStack().getType() == Material.APPLE) {
			
			Player close = CoreUtils.getNearestPlayer(e.getLocation().subtract(0, 1.7, 0), 0.9);
			if(close != null)
				return;
			
			int level = -1;
			for(Entity nearby : e.getEntity().getNearbyEntities(64, 64, 64)) {
				if(nearby.getType() == EntityType.PLAYER) {
					
					Player p = (Player) nearby;
					if(!isEnabled(p))
						continue;
					
					int templevel = getLevel(p);
					if(templevel < unlocked)
						continue;
					
					level = templevel;
					break;
				}
			}
			if(level < 1)
				return;
			
			if(chance.isRandomChance(level)) {
				
				e.getEntity().getItemStack().setType(Material.GOLDEN_APPLE);
				
				if(level >= upgraded && godChance.isRandomChance(level)) {
					e.getEntity().getItemStack().setDurability((short)1);
				}
				
				GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, e.getLocation());
			}
			
		}
	}

}
