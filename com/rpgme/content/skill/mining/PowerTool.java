package com.rpgme.content.skill.mining;

import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.*;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.SimpleCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class PowerTool <S extends Skill> extends Ability<S> {
	
	public interface BlockBreaker {
		
		void onBlockBreak(BlockBreakEvent event);
		
	}

	public static final int ID = Id.newId();

	private int unlocked, upgraded, finalupgrade;

	private final Cooldown cooldown = new SimpleCooldown(plugin);;
	private Scaler cooldownScaler, durationScaler;

	private Collection<Material> materials;
	private String toolSuffix;

	// map holding the end-time for active users;
	private final Map<Player, Long> endtimeMap = new WeakHashMap<>();
	// map holding the face of the block they are breaking
	private final Map<Player, BlockFace> blockfaceMap = new WeakHashMap<>();

	
	public PowerTool(S skill, String toolSuffix, EnumSet<Material> materials, int[] cooldownDuration, int[] abilityDuration) {
		super(skill, "Power Tool", ID);
		this.toolSuffix = toolSuffix;
		this.materials = materials;

		cooldownScaler = new Scaler(unlocked, cooldownDuration[0], 100, cooldownDuration[1]);
		durationScaler = new Scaler(unlocked, abilityDuration[0], 100, abilityDuration[1]);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlocked) {
			list.add("Power Tool duration:" + (forlevel < finalupgrade ? durationScaler.readableScale(forlevel) + 's' : "infinite"));
			list.add("Power Tool cooldown:" + (forlevel < finalupgrade ? cooldownScaler.readableScale(forlevel) + 's' : "n/a"));
		}
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 5)
				.addValue("upgrade1", 50)
				.addValue("upgrade2", 100);
		ConfigHelper.injectNotification(messages, getClass(), 1);
		ConfigHelper.injectNotification(messages, getClass(), 2);
		ConfigHelper.injectNotification(messages, getClass(), 3);
		ConfigHelper.injectMessage(messages, "ability_powertool_deactivated");
		ConfigHelper.injectMessage(messages, "ability_powertool_activated");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);

		unlocked = getConfig().getInt("Power Tool.unlocked", 1);
		upgraded = getConfig().getInt("Power Tool.upgrade1", 50);
		finalupgrade = getConfig().getInt("Power Tool.upgrade2", 100);

		// register notifications
		addNotification(unlocked, Notification.ICON_UNLOCK, getName(), ConfigHelper.getNotification(messages, getClass(), 1, toolSuffix));
		addNotification(upgraded, Notification.ICON_UPGRADE, getName(), ConfigHelper.getNotification(messages, getClass(), 2));
		addNotification(finalupgrade, Notification.ICON_UPGRADE, getName(), ConfigHelper.getNotification(messages, getClass(), 3));
	}

	private boolean isAcceptedItem(ItemStack item) {
		return item != null && item.getType().name().endsWith(toolSuffix);
	}

	private boolean isAcceptedBlock(Player p, Block block) {
		return materials.contains(block.getType()) && PluginIntegration.getInstance().canChange(p, block);
	}
	
	private boolean isAcceptedClick(Action action, Block block) {
		return CoreUtils.isRightClick(action) && (block == null || !CoreUtils.hasClickAction(block.getType()) );
	}
	
	public boolean isActive(Player p) {
		Long endtime = endtimeMap.get(p);
		return endtime != null && (endtime == 0 || endtime > System.currentTimeMillis());
	}


	public void setActive(Player p, boolean active) {
		int level = getLevel(p);
		if(active) {

			if(level < unlocked)
				return;

			String message = messages.getMessage("ability_powertool_activated");
			if(level < 100) {

				message += " &b: "+durationScaler.readableScale(level)+'s';
				endtimeMap.put(p, System.currentTimeMillis() + (long) (durationScaler.scale(level) * 1000L));
			} else {
				endtimeMap.put(p, 0L);
			}
			MessageUtil.sendToActionBar(p, StringUtils.colorize(message));
			GameSound.play(Sound.ENTITY_PLAYER_ATTACK_CRIT, p, 1.2f, 1.5f);

		} else {

			endtimeMap.remove(p);
			blockfaceMap.remove(p);
			
			String message = messages.getMessage("ability_powertool_deactivated");
			
			if(level < finalupgrade) {
				long cooldownDuration = (long) (cooldownScaler.scale(level) * 1000L);
				cooldown.add(p, cooldownDuration);
				message += (" &7cooldown: &b" + cooldownScaler.readableScale(level) + 's');
			}
			
			MessageUtil.sendToActionBar(p, StringUtils.colorize(message));
			GameSound.play(Sound.ENTITY_PLAYER_ATTACK_CRIT, p, 1.2f, 0.6f);
		}
	}

	
	@EventHandler
	public void onActivate(PlayerInteractEvent event) {
		if(!isAcceptedItem(event.getItem()))
			return;
		
		Player p = event.getPlayer();
		if(!isEnabled(p))
			return;

		if(isActive(p)) {
			if(event.getAction() == Action.LEFT_CLICK_BLOCK)
				blockfaceMap.put(p, event.getBlockFace());
			else if(isAcceptedClick(event.getAction(), event.getClickedBlock()))
				setActive(p, false);
			return;
		}

		if(!isAcceptedClick(event.getAction(), event.getClickedBlock()))
			return;

		if(cooldown.isOnCooldown(p)) {
			sendOnCooldownMessage(p, cooldown.getMillisRemaining(p));
			return;
		}

		setActive(p, true);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPowertoolUse(BlockBreakEvent event) {
		Player p = event.getPlayer();
		
		if(!isEnabled(p))
			return;
		
		Long endtime = endtimeMap.get(p);
		if(endtime == null)
			return;
		
		if(endtime > 0 && endtime < System.currentTimeMillis()) {
			setActive(p, false);
			return;
		}
		
		ItemStack tool = p.getInventory().getItemInMainHand();
		if(!isAcceptedBlock(p, event.getBlock()) || !isAcceptedItem(tool))
			return;
		
		BlockFace face = blockfaceMap.remove(p);
		if(face == null) face = CoreUtils.getNearestFace(event.getBlock(), p.getEyeLocation());
		
		boolean reduced = getLevel(p) < upgraded;
		for(Block b : getSurroundingBlocks(event.getBlock(), face, reduced)) {
			
			if(!isAcceptedBlock(p, b))
				continue;
			
			((BlockBreaker)skill).onBlockBreak(new BlockBreakEvent(b, p));
			b.breakNaturally(tool);
			
		}

	}

	public static Collection<Block> getSurroundingBlocks(Block clicked, BlockFace face, boolean reduced) {
		int[][] pattern;
	
		switch(face) {
		case UP:
		case DOWN: pattern = PowerTool.cylFlat; break;
		case EAST:
		case WEST: pattern = PowerTool.cylVertical2; break;
		case NORTH:
		case SOUTH: pattern = PowerTool.cylVertical; break;
		default: pattern = PowerTool.cylFlat;
		}
	
		Set<Block> set = new HashSet<>();
		set.add(clicked);
	
		if(reduced) {
			for(int i : PowerTool.reduced) {
				int[] arr = pattern[i];
				set.add(clicked.getRelative(arr[0], arr[1], arr[2]));
			}
		}
		else {
			for(int[] arr : pattern) {
				set.add(clicked.getRelative(arr[0], arr[1], arr[2]));
			}
		}
		return set;
	}
	
	public static final int[] reduced = new int[] { 1, 3, 4, 6 };

	public static final int[][] cylVertical2 = {
	new int[]{ 0,1,-1 }, new int[]{ 0,1,0 }, new int[]{ 0,1,1 },
	new int[]{ 0,0,-1 }, 					new int[]{ 0,0,1 },
	new int[]{ 0,-1,-1 }, new int[]{ 0,-1,0 }, new int[]{ 0,-1,1 } };

	public static final int[][] cylVertical = {
	new int[]{ -1,1,0 }, new int[]{ 0,1,0 }, new int[]{ 1,1,0 },
	new int[]{ -1,0,0 }, 					new int[]{ 1,0,0 },
	new int[]{ -1,-1,0 }, new int[]{ 0,-1,0 }, new int[]{ 1,-1,0 }};

	public static final int[][] cylFlat = new int[][] { 
	new int[]{ -1,0,1 }, new int[]{ 0,0,1 }, new int[]{ 1,0,1 },
	new int[]{ -1,0,0 }, 					new int[]{ 1,0,0 },
	new int[]{ -1,0,-1 }, new int[]{ 0,0,-1 }, new int[]{ 1,0,-1 }};
	

}
