package com.rpgme.content.module.trails;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GUI;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by Robin on 08/02/2017.
 */
public class TrailMenu extends GUI<RPGme> {

    private final SkillTrails manager;
    private final List<Integer> list = Lists.newArrayList();

    public TrailMenu(SkillTrails manager, Player player) {
        super(manager.plugin, player, "Trail selector", 2);
        this.manager = manager;
        this.addItems();
    }

    public ItemStack getItemRepresentation(Skill skill) {
        String name = "&a" + skill.getDisplayName() + " Mastery";
        return ItemUtils.create(skill.getItemRepresentation(), name, "");
    }

    public ItemStack getCancelItem() {
        return ItemUtils.create(Material.REDSTONE_BLOCK, "&cOff", "Remove your trail");
    }

    @Override
    public void addItems() {
        int required = manager.masteryLevel;
        RPGPlayer rp = plugin.getPlayer(player);

        for(Skill skill : plugin.getSkillManager().getEnabledSkills()) {
            if(rp.getLevel(skill) >= required) {
                addItem(list.size(), getItemRepresentation(skill));
                list.add(skill.getId());
            }
        }

        addItem(17, getCancelItem());
    }

    @Override
    public void doAction(int slot) {
        if(slot == 17) {
            manager.setTrail(player, null);
            GameSound.play(Sound.UI_BUTTON_CLICK, player, 1.0F, 0.7F);
        } else {
            manager.setTrail(player, ParticleTrail.valueOf(list.get(slot)));
            GameSound.play(Sound.UI_BUTTON_CLICK, this.player.getLocation(), 1.0F, 1.2F);
        }

    }
}
