package com.rpgme.content.skill.stamina;

import com.rpgme.plugin.event.RPGLevelupEvent;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.PermissionChecker;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import java.util.List;


/**
 *
 */
public class MaxHealth extends Ability<Stamina> {

    private Scaler healthBoost;

    public MaxHealth(Stamina skill) {
        super(skill, "Max Health", Stamina.ABILITY_MAX_HEALTH);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        config.addValue("Hearts at level 1", "start with", 8)
                .addValue("Hearts at target level", "end with", 20);
        // the LegacyConfig class is for this plugin only. It is to transfer over configurations from a previous version.
        // other plugins should just write to the builders directly.
        ConfigHelper.injectMessage(messages, "notification_stamina_increasedhealth", "increased");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        int start = config.getInt("start with");
        int end = config.getInt("end with");
        int endlevel = skill.getManager().getTargetLevel();

        healthBoost = new Scaler(0, start, endlevel, end);

        int hearts = start;
        for(int i = 2; i < endlevel; i++) {

            int newHearts = getHeartsAt(i);
            if(newHearts > hearts) {
                hearts = newHearts;
                String message = messages.getMessage("increased");
                addNotification(Notification.builder().title(getName()).sticky(false).icon(Notification.ICON_UPGRADE).text(message).build());
            }
        }
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if(healthBoost != null)
            list.add("Health:"+String.format("%+d", getHeartsAt(forlevel)-10));
    }

    public void updateMaxHealth(RPGPlayer player) {
        int level = getLevel(player);
        int hearts = getHeartsAt(level);
        player.getPlayer().setMaxHealth(hearts * 2);
    }

    private int getHeartsAt(int level) {
        return healthBoost != null ? (int) Math.round(healthBoost.scale(level)) : 10;
    }


    @EventHandler
    public void onLogin(RPGPlayerJoinEvent e) {
        updateMaxHealth(e.getRPGPlayer());
    }

    @EventHandler
    public void onStaminaLevelup(RPGLevelupEvent e) {
        if(e.getSkill() == getSkill() && healthBoost != null) {

            int currenthearts = getHeartsAt(e.getFromLevel());
            int newhearts = getHeartsAt(e.getToLevel());

            e.getPlayer().setMaxHealth(newhearts * 2);
            if(newhearts > currenthearts) {
                e.getPlayer().setHealth(Math.min(newhearts * 2, e.getPlayer().getHealth() +2));
            }
        }
    }

    /**
     * Change health if player moves to or from a disabled world
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onWorldChange(PlayerChangedWorldEvent event) {
        if(!isEnabled(event.getPlayer())) return;

        boolean fromRPGme = !PermissionChecker.isDisabledWorld(event.getFrom());
        boolean toRPGme = !PermissionChecker.isInDisabledWorld(event.getPlayer());

        if(fromRPGme && !toRPGme) {
            event.getPlayer().setMaxHealth(20); // vanilla default (10 hearts)
        }
        else if(!fromRPGme && toRPGme) {
            updateMaxHealth(getPlayer(event.getPlayer()));
        }
    }

}
