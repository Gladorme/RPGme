package com.rpgme.content.skill.farming;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.blockmeta.BlockDataListener;
import com.rpgme.plugin.blockmeta.BlockDataManager;
import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.util.CoreUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.List;

/**
 *
 */
public class FarmingBlockListener extends BlockDataListener {
	
	public static final String LAYER_NAME = "Farming";
		
	private final Farming skill;
    private final BlockDataManager dataStore;
	
	public FarmingBlockListener(Farming skill) {
		super(skill.getPlugin(), LAYER_NAME);
		this.skill = skill;

        dataStore = plugin.getModule(RPGme.MODULE_BLOCK_META);
        dataStore.addEmptyLayer(KEY, String.class);
	}
	
	public void setString(Block block, Object string) {
		dataStore.set(block, LAYER_NAME, string);
	}
		
	public String collectString(Block block) {
		return dataStore.collect(block, LAYER_NAME, String.class);
	}

	public String getString(Block block) {
	    return (String) dataStore.getLayer(block.getWorld(), LAYER_NAME).getData(block);
    }

	public void setPlayerByPlaced(Block block, boolean placed) {
		dataStore.set(block, PlayerPlacedListener.PLAYER_PLACED, placed);
	}
	
	public boolean collectPlacedByPlayer(Block block) {
		return dataStore.collect(block, PlayerPlacedListener.PLAYER_PLACED, Boolean.class);
	}
	
	
	public boolean isGroundBlock(Block block) {
		return isGroundBlock(block.getType());
	}
	public boolean isGroundBlock(Material t) {
		return t == Material.GRASS || t == Material.DIRT || t == Material.SOIL || t == Material.SOUL_SAND || t == Material.SAND;
	}

	@Override
	protected void onBreak(Block block, Player player) {
		if(block.isEmpty()) return;
		
		if(isGroundBlock(block)) {
			skill.handleBlockBreak(block.getRelative(BlockFace.UP), player);
		} else {
			skill.handleBlockBreak(block, player);
		}
	}

	@Override
	protected void onPiston(List<Block> blocks, BlockFace direction) {
		for(Block block : blocks) {
			onBreak(block, CoreUtils.getNearestPlayer(block.getLocation(), 14));
		}
	}
	
	
	
}
