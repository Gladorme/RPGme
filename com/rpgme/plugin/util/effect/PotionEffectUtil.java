package com.rpgme.plugin.util.effect;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionEffectUtil {
	
	private static final int inf = 1728000;

	public static void addInfiniteEffect(Player p, PotionEffectType type, int level) {
		addInfiniteEffect(p, type, level, true);
	}
	
	public static void addInfiniteEffect(Player p, PotionEffectType type, int level, boolean force) {
		p.addPotionEffect(new PotionEffect(type, inf, level-1, true), force);
	}
	
	public static void addPotionEffect(Player p, PotionEffectType type, int level, double seconds, boolean particles) {
		addPotionEffect(p, type, level, seconds, false, particles);
	}
	
	public static void addPotionEffect(Player p, PotionEffectType type, int level, double seconds, boolean particles, boolean force) {
		p.addPotionEffect(new PotionEffect(type, (int) (seconds*20), level-1, false, particles), force);
	}
}
