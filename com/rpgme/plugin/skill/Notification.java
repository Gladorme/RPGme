package com.rpgme.plugin.skill;

import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.Symbol;

import static com.rpgme.plugin.util.Symbol.STAR_FULL;

/**
 *
 */
public class Notification implements Comparable<Notification> {

	public static class Builder {

		int level = LVL_NONE;
		String icon, title, text;

		boolean sticky = true;

		public Notification build() {
			return new Notification(level, icon, title, text, sticky);
		}

        /**
         * @param level the level at which this should appear. Use {@link #LVL_NONE} to make it always appear, and without a level.
         * @return this for chaining
         */
		public Builder level(int level) {
			this.level = level;
			return this;
		}

        /**
         * @param icon A String or character to display as icon. For consitstency use one of {@link #ICON_UNLOCK}, {@link #ICON_UPGRADE} or {@link #ICON_PASSIVE}
         * @return this for chaining
         */
		public Builder icon(String icon) {
			this.icon = icon;
			return this;
		}

        /**
         * @param title The title for this notification
         * @return this for chaining
         */
		public Builder title(String title) {
			this.title = title;
			return this;
		}

        /**
         * @param message The body of the notification. This should come from a messages bundle so it can be translated by users
         * @return this for chaining
         */
		public Builder text(String message) {
			this.text = message;
			return this;
		}

        /**
         * @param sticky flag to indicate it should be shown in /<skill> command overviews. Default true.
         * @return this for chaining
         */
		public Builder sticky(boolean sticky) {
			this.sticky = sticky;
			return this;
		}
	}

	public static Notification.Builder builder() {
		return new Builder();
	}

	public static final String
			ICON_UNLOCK = "✪",
			ICON_UPGRADE = String.valueOf(STAR_FULL),
			ICON_PASSIVE = "✉";

	public static String upgradableIcon(int step, int max) {
        return org.apache.commons.lang3.StringUtils.repeat(STAR_FULL, step) + org.apache.commons.lang3.StringUtils.repeat(Symbol.STAR_EMPTY, max - step);
    }

	public static final int LVL_NONE = Integer.MAX_VALUE;

	private final int level;
    private final String icon, title, text;
    private final boolean sticky;

    /**
     * Constructs a new Notification object
     * @param level the level at which this should appear. Use {@link #LVL_NONE} to make it always appear, and without a level.
     * @param icon A String or character to display as icon. For consitstency use one of {@link #ICON_UNLOCK}, {@link #ICON_UPGRADE} or {@link #ICON_PASSIVE}
     * @param title The title for this notification
     * @param text The body of the notification. This should come from a messages bundle so it can be translated by users
     * @param sticky flag to indicate it should be shown in /<skill> command overviews. Default true.
     */
	public Notification(int level, String icon, String title, String text, boolean sticky) {
		this.sticky = sticky;
		this.text = StringUtils.colorize(text);
		this.title = title;
		this.icon = icon;
		this.level = level;
	}

    /**
     * Constructs a new Notification object
     * @param level the level at which this should appear. Use {@link #LVL_NONE} to make it always appear, and without a level.
     * @param icon A String or character to display as icon. For consitstency use one of {@link #ICON_UNLOCK}, {@link #ICON_UPGRADE} or {@link #ICON_PASSIVE}
     * @param title The title for this notification
     * @param text The body of the notification. This should come from a messages bundle so it can be translated by users
     */
	public Notification(int level, String icon, String title, String text) {
        this(level, icon, title, text, true);
    }

	public int getLevel() {
        return Math.max(1, level);
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public String getText() {
		return text == null ? "" : text;
    }

	public boolean isSticky() {
        return sticky;
    }

	public String getIcon() {
		return icon == null ? "" : icon;
	}

	@Override
    public int compareTo(Notification arg0) {
        return Integer.compare(level == LVL_NONE ? -1 : level, arg0.getLevel());
    }

}
