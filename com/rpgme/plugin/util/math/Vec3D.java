package com.rpgme.plugin.util.math;

import org.bukkit.util.Vector;

public class Vec3D {
	
	public final double x,y,z;
	
	public Vec3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3D(Vec3D vec) {
		this(vec.x, vec.y, vec.z);
	}
	
	public Vec3D(Vector loc) {
		this(loc.getX(), loc.getY(), loc.getZ());
	}
	
	public Vec3D() {
		this(0.0, 0.0, 0.0);
	}
	
	public Vec3D add(Vec3D vec) {
		return new Vec3D(x + vec.x, y + vec.y, z + vec.z);
	}
	
	public Vec3D add(double x, double y, double z) {
		return new Vec3D(this.x + x, this.y + y, this.z + z);
	}
	
	public Vec3D substract(Vec3D vec) {
		return new Vec3D(x - vec.x, y - vec.y, z - vec.z);
	}
	public Vec3D substract(double x, double y, double z) {
		return new Vec3D(this.x - x, this.y - y, this.z - z);
	}
	
	public Vec3D scale(double scale) {
		return new Vec3D(x * scale, y * scale, z * scale);
	}
		
	public double distanceSquared(Vec3D other) {
		return Math.abs( substract(other).lengthSquared() );
	}
	
	public double distance(Vec3D other) {
		return Math.sqrt(distanceSquared(other));
	}
	
	public double lengthSquared()
	{
		return x * x + y * y + z * z;
	}

	public double length()
	{
		return Math.sqrt(lengthSquared());
	}
	
	public Vec3D normalise()
	{
		return scale(1.0f / length());
	}
	
	public static double dot(Vec3D a, Vec3D b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}
	
	@Override
	public String toString()
	{
		return "Vector[x=" + x + ", y=" + y + ", z=" + z + "]";
	}
	
	public boolean equals(Vec3D other) {
		return (equals(x, other.x) && equals(y, other.y) && equals(z, other.z));
	}
	
	@Override
	public Vec3D clone() {
		return new Vec3D(this);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		return equals((Vec3D)obj); 
	}
	
	@Override
	public int hashCode() {
		return (int) (x*y*z);
	}

	private static boolean equals(double a, double b) {
		return Math.abs(a - b) < 0.0001;
	}
	
	
	
	
	
	

}
