package com.rpgme.plugin.command;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class RPGmeCommand extends CoreCommand {

    private static final String
            USAGE_SET = "/rpg set <player> <skill> <level>",
            USAGE_ADDLEVEL = "/rpg addlevel <player> <skill> <amount>",
            USAGE_SETALL = "/rpg setall <player> <level>",
            USAGE_SETEXP = "/rpg setexp <player> <skill> <exp>",
            USAGE_ADDEXP = "/rpg addexp <player> <skill> <exp>";

    private static final String
            PERMISSION_SETEXP = "rpgme.command.setexp",
            PERMISSION_RELOAD = "rpgme.reload";

	public RPGmeCommand(RPGme plugin) {
		super(plugin, "rpgme", "rpgme.setexp", "rpgme.save", "rpgme.reload");
		setConsoleAllowed(true);
		setShowHelp(false);
		
		setAliases("rpg");
		setDescription("Some admin features and command help");
        setCommandHelp(new CommandHelp.Builder()
                .addUsage("/rpg commands", "List all available commands")
                .addUsage("/rpg reload", "Add levels to a skill", PERMISSION_RELOAD)
                .addUsage(USAGE_SET, "Set a level to a new value", PERMISSION_SETEXP)
                .addUsage(USAGE_ADDLEVEL, "Add levels to a skill", PERMISSION_SETEXP)
                .addUsage(USAGE_SETALL, "Set the level of all skills to a new value", PERMISSION_SETEXP)
                .addUsage(USAGE_SETEXP, "Set the exp of skill to a specific value", PERMISSION_SETEXP)
                .addUsage(USAGE_ADDEXP, "Add exp for a skill to a player", PERMISSION_SETEXP)
                .build());
	}

    @Override
	public void execute(CommandSender sender, String alias, List<String> flags) {

		if(flags.isEmpty() || flags.remove("help") || flags.contains("?")) {

			plugin.getCommandManager().printCommandHelp(this, sender);
			
		} else if(flags.remove("commands")) {

		    plugin.getCommandManager().printHelp(sender);

        } else if(flags.remove("set")) {

            handleSetLevel(sender, flags, false);

		} else if(flags.remove("addlevel")) {

		    handleSetLevel(sender, flags, true);

        } else if(flags.remove("setexp")) {

            handleSetExp(sender, flags, false);

        } else if(flags.remove("addexp")) {

			handleSetExp(sender, flags, true);
			
		} else if(flags.remove("setall")) {
			
			handleSetAllLevels(sender, flags);

		} else if(flags.remove("reload")) {
			
			if(hasPermission(sender, PERMISSION_RELOAD)) {
                plugin.reloadConfig();
                sender.sendMessage(StringUtils.colorize("&aRPGme (&e" + plugin.getDescription().getVersion() + "&a) - Files reloaded."));
            }
			
		} else {
			plugin.getCommandManager().printCommandHelp(this, sender);
		}
		
	}
	
	
	private void handleSetAllLevels(CommandSender sender, List<String> flags) {
		if(!hasPermission(sender, PERMISSION_SETEXP)) {
			return;
		}

		if(flags.size() < 2) {
			error(sender, USAGE_SETALL);
			return;
		}
		
		Player target = plugin.getServer().getPlayer(flags.get(0));

		if(target == null) {
			error(sender, "Error first argument: Targer player '"+flags.get(0)+"' not found.");
			return;
		}
		
		int newvalue;

		try {
			newvalue = ExpTables.xpForLevel(Integer.parseInt(flags.get(1))) + 1;
		} catch(NumberFormatException e) {
			error(sender, "Error third argument: '"+flags.get(1)+"' is not a number.");
			return;
		}
		
		RPGPlayer rp = plugin.getPlayer(target);
		for(int skill : plugin.getSkillManager().getKeys()) {
			rp.setExp(skill, newvalue);
		}
		rp.getSkillSet().recalculateTotalLevel();
		rp.updateScoreboard();
		
		sender.sendMessage(ChatColor.YELLOW + " All levels of "+target.getName() + " set to " + flags.get(1));
	}

	private void handleSetLevel(CommandSender sender, List<String> flags, boolean add) {
        if(!hasPermission(sender, PERMISSION_SETEXP)) {
            return;
        }

        if(flags.size() < 3) {
            error(sender, USAGE_SET + " or " + USAGE_ADDLEVEL);
            return;
        }

        Player target = plugin.getServer().getPlayer(flags.get(0));

        if(target == null) {
            error(sender, "Error first argument: Target player '"+flags.get(0)+"' not found.");
            return;
        }

        Skill skill = plugin.getSkillManager().getByName(flags.get(1));

        if(skill == null) {
            error(sender, "Error second argument: Skill '"+flags.get(1)+"' not recognized.");
            return;
        } else if(!skill.isEnabled(target)) {
            error(sender, target.getName() + " does not have permission to use that skill.");
            return;
        }

        int amount;

        try {
            amount = Integer.parseInt(flags.get(2));
        } catch(NumberFormatException e) {
            error(sender, "Error third argument: '"+flags.get(2)+"' is not a number.");
            return;
        }

        RPGPlayer rp = plugin.getPlayer(target);
        float currentExp = rp.getExp(skill);

        if(add) {
            int newExp = ExpTables.xpForLevel(rp.getLevel(skill.getId()) + amount) + 1;

            if(newExp < 1) {
                error(sender, "Invalid exp value: new level cannot be negative");
                return;
            }

            rp.setExp(skill.getId(), newExp);
            sender.sendMessage(ChatColor.YELLOW.toString() + (newExp - currentExp) + " " + skill.getDisplayName() + " exp added to player "+target.getName());
        }
        else {
            if(amount < 1) {
                error(sender, "Invalid exp value: new level cannot be negative");
                return;
            }

            amount = ExpTables.xpForLevel(amount) + 1;
            rp.setExp(skill.getId(), amount);
            sender.sendMessage(ChatColor.YELLOW + skill.getDisplayName() + " exp changed from " + (int) currentExp + " to " + amount + " for player "+target.getName());
        }

        rp.getSkillSet().recalculateTotalLevel();
        rp.updateScoreboard();
    }
	
	private void handleSetExp(CommandSender sender, List<String> flags, boolean add) {
		if(!hasPermission(sender, PERMISSION_SETEXP)) {
			return;
		}

		if(flags.size() < 3) {
			error(sender, USAGE_SETEXP + " or " + USAGE_ADDEXP);
			return;
		}

		Player target = plugin.getServer().getPlayer(flags.get(0));

		if(target == null) {
			error(sender, "Error first argument: Target player '"+flags.get(0)+"' not found.");
			return;
		}

		Skill skill = plugin.getSkillManager().getByName(flags.get(1));

		if(skill == null) {
			error(sender, "Error second argument: Skill '"+flags.get(1)+"' not recognized.");
			return;
		} else if(!skill.isEnabled(target)) {
			error(sender, target.getName() + " does not have permission to use that skill.");
			return;
		}

		float amount;

		try {
			amount = Float.parseFloat(flags.get(2));
		} catch(NumberFormatException e) {
			error(sender, "Error third argument: '"+flags.get(2)+"' is not a number.");
			return;
		}

		RPGPlayer rp = plugin.getPlayer(target);
        float currentExp = rp.getExp(skill);
		
		if(add) {
		    if(currentExp + amount < 1) {
                error(sender, "Invalid exp value: new level cannot be negative");
                return;
            }

			rp.setExp(skill.getId(), currentExp + amount);
			sender.sendMessage(ChatColor.YELLOW.toString() + amount + " " + skill.getDisplayName() + " exp added to player "+target.getName());
		}
		else {
		    if(amount < 1) {
                error(sender, "Invalid exp value: new level cannot be negative");
                return;
            }

			rp.setExp(skill.getId(), amount);
			sender.sendMessage(ChatColor.YELLOW + skill.getDisplayName() + " exp changed from " + (int) currentExp + " to " + amount + " for player "+target.getName());
		}
		
		rp.getSkillSet().recalculateTotalLevel();
		rp.updateScoreboard();

	}

    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        return null;
    }
}
