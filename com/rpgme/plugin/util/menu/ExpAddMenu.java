package com.rpgme.plugin.util.menu;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GUI;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.Symbol;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ExpAddMenu extends GUI<RPGme> {
	
	private final int amount, minlvl;
	private Map<Skill, Integer> map;
    private List<Skill> availableSkills;
    private ItemStack item;

    public ExpAddMenu(RPGme plugin, Player p, ItemStack item, int exp, int minlevel) {
        this(plugin, p, item, exp, minlevel, null);
    }

	public ExpAddMenu(RPGme plugin, Player p, ItemStack item, int exp, int minlevel, List<Skill> skills) {
		super(plugin, p, "Exp Tomb", 2);
		this.amount = exp;
		this.minlvl = minlevel;
        this.availableSkills = skills != null ? skills : new ArrayList<>(plugin.getSkillManager().getEnabledSkills(p));
        this.item = item;
		
		addItems();
	}

	@Override
	public void addItems() {
		map = new HashMap<>(plugin.getSkillManager().getSkillCount());
		int slot = 1;
		
		for(Skill skill : availableSkills) {
			if(skill.isEnabled(player)) {
				
				map.put(skill, slot);
				addItem(slot++, getItemRepresentation(skill));
				if(slot == 8)
					slot = 10;
			}
		}
		
	}

	@Override
	public void doAction(int slot) {
        Skill skill = null;
        for(Entry<Skill, Integer> e : map.entrySet()) {
            if(e.getValue() == slot) {
                skill = e.getKey();
            }
        }
        if(skill == null)
            return;

		if(!canUse(skill)) {
			player.sendMessage(plugin.getMessages().getMessage("ui_err_needhigherlevel", minlvl, skill.getDisplayName()));
			GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
		}
		else {
			
			this.closeGUI();

            final Skill finalSkill = skill;

			new ConfirmationScreen(plugin, player, "&fIncrease skill &e"+skill.getDisplayName()+"?") {

				@Override
				public void onConfirm() {

                    PlayerInventory inv = player.getInventory();
				    if(inv.getItemInMainHand().equals(item)) {
                        ItemStack newitem = player.getInventory().getItemInMainHand();
                        newitem.setAmount(newitem.getAmount()-1);

                        player.getInventory().setItemInMainHand(newitem.getAmount() > 0 ? newitem : null);
                    }
                    else if(inv.getItemInOffHand().equals(item)) {
                        ItemStack newitem = player.getInventory().getItemInOffHand();
                        newitem.setAmount(newitem.getAmount()-1);

                        player.getInventory().setItemInOffHand(newitem.getAmount() > 0 ? newitem : null);
                    }
                    else {
                        GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
                        return;
                    }

					GameSound.play(Sound.BLOCK_NOTE_HARP, player, 2f, 1.5f);
					((RPGme)plugin).getPlayer(player).addTrueExp(finalSkill, amount);
				}

				@Override
				public void onDeny() {
					GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
				}
			}.openGUI();
			
		}
		
	}
	
	private boolean canUse(Skill skill) {
		return skill.isEnabled(player) && plugin.getLevel(player, skill.getId()) >= minlvl;
	}

	
	private ItemStack getItemRepresentation(Skill skill) {
		String name = "&a" + skill.getDisplayName();
		String lore;
		
		RPGPlayer p = plugin.getPlayer(player);
		int level = p.getLevel(skill);
		
		if(level >= minlvl) {
			
			lore =  "&a" + Symbol.YES + "&e +"+amount+" exp \n &7"+level;
			
			int newlvl = ExpTables.getLevelAt(p.getExp(skill)+amount);
			if(newlvl > level) {
				lore += " &2" + Symbol.ARROW_RIGHT2 + " " + newlvl;
			}
		}
		
		else {
			lore = "&c" + Symbol.NO + "&7 not level "+minlvl;
		}

		Material mat = skill.getItemRepresentation();
		if(mat == null) mat = Material.WORKBENCH;
		return ItemUtils.addItemFlags(ItemUtils.create(mat, name, lore), ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS);
	}
	
	
	

}
